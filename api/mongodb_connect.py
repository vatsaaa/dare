from pymongo import MongoClient
from flask import jsonify, Blueprint, request
import os
import json
import sys

def connect_to_mongo():
   try:
       client = MongoClient(
           "mongodb+srv://dare-dbo:dare1234@dare.po7ih.mongodb.net/?retryWrites=true&w=majority")
       db = client.get_database("DareDataScrap").get_collection("BasicNftDetails").find_one()
       print(db)
   except Exception as e:
       print(e)


def insert_to_mongo():
   try:
       client = MongoClient(
           "mongodb+srv://dare-dbo:dare1234@dare.po7ih.mongodb.net/?retryWrites=true&w=majority")
       db = client.get_database("DareDataScrap").get_collection("NFTCollections")
       f = open(os.path.join(sys.path[0], "mock_data{}AllCollectionsData1.json".format(os.sep)), "r")
       print(f)
       data = json.load(f)
       f.close()
       #data1 = jsonify(data)
       print("loaded")
       db.insert_many(data)
       print("inserted")
   except Exception as e:
       print(e)


def update_to_mongo():
    client = MongoClient("mongodb+srv://dare-dbo:dare1234@dare.po7ih.mongodb.net/?retryWrites=true&w=majority")
    db = client.get_database("DareDataScrap").get_collection("NFTCollections_New")
    db.update_many({}, {"$set": {"Description": "Okay Bears is a culture shift. A clean collection of 10,000 diverse bears building a virtuous community that will transcend the internet into the real world."}}, upsert=False, array_filters=None)


if __name__ == "__main__":
    update_to_mongo()