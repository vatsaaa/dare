import os
import logging.config

if not os.path.exists('./logs'):
    os.mkdir('./logs')

logger = logging.getLogger()