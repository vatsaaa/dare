from utils.LoggingConfig import logger

class DatabaseContext:
    def __init__(self,db):
        logger.debug("Database Context for Dare")
        self.db = db

    def get(self, collection:str, body:dict, key=None):
        found = self.db.get(collection=collection, body=body , key=key)
        return found

    def set(self, collection:str, key , value):
        return self.db.set(collection=collection, key=key, value=value)

    def delete(self, collection:str, key):
        return self.db.delete(collection=collection, key=key)

    def get_with_limit(self, collection:str, limit=0, sort={}, body={}, key=None):
        return self.db.get_with_limit(collection=collection, key=key, limit=limit, sort=sort, body=body)