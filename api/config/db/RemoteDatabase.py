from pymongo import MongoClient, errors
from interface import implements

from config.db.IDatabase import IDatabase


class RemoteDatabase(implements(IDatabase)):
    def __init__(self, collections: dict):
        try:
            super().__init__()
            mongo_client = MongoClient(
                "mongodb+srv://dare-dbo:dare1234@dare.po7ih.mongodb.net/?retryWrites=true&w=majority")
            self.db = mongo_client.get_database("DareDataScrap")
            self.collections = collections

        except errors as e:
            raise Exception("Error while connecting MongoDb: {}".format(e))
        except Exception as e:
            raise Exception("Unknown Error while connecting MongoDb: {}".format(e))

    def get(self, collection: str, body: dict, key=None):
        collections = self.collections
        if isinstance(collection, str):
            try:
                db_collection = self.db.get_collection(collections[collection])
                result = db_collection.find(body)
            except Exception as e:
                raise Exception("Error while getting record from collection {} : {}".format(collection, e))
            else:
                return result

        else:
            return "Invalid Collection type"

    def get_with_limit(self, collection:str, limit:int=0, sort:dict={}, body:dict={}, key=None):
        collections = self.collections
        if isinstance(collection, str):
            try:
                db_collection = self.db.get_collection(collections[collection])
                if sort:
                    result = db_collection.find().limit(limit).sort(sort)
                elif body:
                    result = db_collection.find(body).limit(limit)
                else:
                    result = db_collection.find().limit(limit)
            except Exception as e:
                raise Exception("Error while getting record from collection {} : {}".format(collection, e))
            else:
                return result

        else:
            return "Invalid Collection type"

    def set(self, collection: str, key, value):
        collections = self.collections
        if isinstance(collection, str):
            try:
                db_collection = self.db.get_collection(collections[collection])
                if key is None:
                    result = db_collection.update_one(value)

                else:
                    result = db_collection.update_one({'_id': key}, value)
            except Exception as e:
                raise Exception("Error while getting record from collection {} : {}".format(collection, e))
            else:
                return result

        else:
            return "Invalid Collection type"

    def delete(self, collection:str, key):
        return "delete successful"
