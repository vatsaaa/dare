from interface import Interface

class IDatabase(Interface):
    def get(self,collection:str,body:dict,key=None):
        pass

    def set(self, collection:str, key, value):
        pass

    def delete(self, collection:str, key):
        pass

    def get_with_limit(self, collection:str, limit:int =0, sort:dict={}, body:dict={}, key=None):
        pass
