import os
import abc
from config.db.DatabaseContext import DatabaseContext
from config.db.RemoteDatabase import RemoteDatabase

from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
from flask_cors import CORS
from config.app_logger import *


class Singleton(type):
    _instance={}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instance:
            cls._instance[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instance[cls]


class Config(abc.ABC):
    def __init__(self, db):
        self.db = db


class EnvConfig(Config):
    def __init__(self, mongo_client):
        super().__init__(mongo_client)


#Flask App Properties
swagger_url = '/swagger'
prefix = "/dare"

api_url ='/config/static/swagger.json'


#app = Flask(__name__, static_url_path='/config/static')
app = Flask(__name__)


swagger_ui_blueprint = get_swaggerui_blueprint(swagger_url, api_url, config={
    'app_name': 'dare'
})
app.register_blueprint(swagger_ui_blueprint, url_prefix=swagger_url)
CORS(app)


collections = dict(
    nft_collection_details = "NFTCollections",
    collection_details = "NFTCollections_New",
    upcoming_collections="UpcomingCollections",
    nft_details = "NftDetails"
)

app_config = EnvConfig(DatabaseContext(RemoteDatabase(collections=collections)))
