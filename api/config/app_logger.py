import logging
from logging.handlers import RotatingFileHandler

rfh = logging.handlers.TimedRotatingFileHandler(
    filename='logs/appLog.log',
    when='d',
    interval=2,
    backupCount=2,
    encoding=None,
    delay=False
)

logging.basicConfig(
    level=logging.ERROR,
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    handlers=[
        rfh
    ]
)

logger = logging.getLogger('main')