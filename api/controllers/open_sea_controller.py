from flask import jsonify, Blueprint, request
from flask_cors import cross_origin
from config.config import prefix
import requests
import logging

asset_api = Blueprint('asset_api', __name__, url_prefix=prefix)


@asset_api.route('assets', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_assets():
    response = requests.get('https://api.opensea.io/api/v1/bundles?limit=20')
    if response.status_code != 200:
        logging.getLogger().error("Error on API: StatusCode {} , {}".format(response.status_code, response.raw))
        return jsonify("Error while calling open sea API's")
    return jsonify(response.json()['bundles'])


@asset_api.route('assets/<asset_id>', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_assets_by_id(asset_id):
    print(asset_id)
    url = 'https://api.opensea.io/api/v1/asset/{0}/1'.format(asset_id)
    response = requests.get(url)
    if response.status_code != 200:
        return jsonify("Error while calling open sea API's")
    return jsonify(response.json())