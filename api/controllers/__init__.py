from config.config import app, prefix

from controllers.open_sea_controller import asset_api
from controllers.user_controller import user_api
from controllers.nft_contoller import nft_api
app.register_blueprint(asset_api)
app.register_blueprint(user_api)
app.register_blueprint(nft_api)
