from flask import jsonify, Blueprint, request
from flask_cors import cross_origin
from config.config import prefix, app_config
import json
import os
import sys
from bson.json_util import dumps
from bson.json_util import loads
import requests
import logging

nft_api = Blueprint('nft_api', __name__, url_prefix=prefix)


@nft_api.route('nft/<nft_id>', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_nft_details_by_id(nft_id):
    # Opening JSON file
    f = open(os.path.join(sys.path[0], "mock_data{}nft.json".format(os.sep)), "r")

    # returns JSON object as
    # a dictionary
    data = json.load(f)
    f.close()
    return jsonify(data[int(nft_id)])


@nft_api.route('nft', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_nfts():
    f = open(os.path.join(sys.path[0], "mock_data{}nft.json".format(os.sep)), "r")

    # returns JSON object as
    # a dictionary
    data = json.load(f)
    f.close()
    return jsonify(data)


@nft_api.route('collection-details/<id>', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_collection_details(id):
    body = {"ID": id}
    print(body)
    collection_info = app_config.db.get(collection="nft_details", body=body)
    return dumps(collection_info)


@nft_api.route('top-collections/', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_top_collection():
    body={
        "ID":{
            '$lt':49,
            '$gt':38
        }
    }
    collection_info = app_config.db.get_with_limit(collection="collection_details", limit=10 , body=body)
    return dumps(collection_info)


@nft_api.route('top-upcoming-collections', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_top_upcoming_collection():
    collection_info = app_config.db.get_with_limit(collection="upcoming_collections", limit=7)
    return dumps(collection_info)


@nft_api.route('wishlist/<user_id>', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_user_wishlist(user_id):
    body = {
        "ID": {
            '$lt': 38,
            '$gt': 30
        }
    }
    collection_info = app_config.db.get_with_limit(collection="collection_details", limit=10, body=body)
    return dumps(collection_info)


@nft_api.route('recently-viewed/<user_id>', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_recently_viewed(user_id):
    body = {
        "ID": {
            '$lt': 23,
            '$gt': 15
        }
    }
    collection_info = app_config.db.get_with_limit(collection="collection_details", limit=10, body=body)
    return dumps(collection_info)


@nft_api.route('top-nft', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_top_nft():
    collection_info = app_config.db.get_with_limit(collection="nft_details", limit=10)
    return dumps(collection_info)