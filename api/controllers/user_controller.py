from functools import wraps

from flask import jsonify, Blueprint, request, make_response,  Flask
from flask_cors import cross_origin
from config.config import prefix
import jwt
from datetime import datetime, timedelta

user_api = Blueprint('user_api', __name__, url_prefix=prefix)
app = Flask(__name__)
app.config['SECRET_KEY'] = 'topsecret'


@user_api.route('users', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_all_users():
    return "get all users"


@user_api.route('users/<user_id>', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_user_by_id(user_id):
    return "User Details"


@user_api.route('createUser', methods=['POST'])
@cross_origin(supports_credentials=True)
def create_user():
    return "Created User Details"


@user_api.route('updateUser/user_id', methods=['PUT'])
@cross_origin(supports_credentials=True)
def update_user(user_id):
    return "Updated User Details"


@user_api.route('deleteUser/<user_id>', methods=['DELETE'])
@cross_origin(supports_credentials=True)
def delete_user(user_id):
    return "Updated User Details"


@user_api.route('/login', methods=['POST', 'GET'])
@cross_origin(supports_credentials=True)
def sign_in_user():
    print(request.form)
    user_details = request.form
    email, password = user_details.get('email'), user_details.get('password')
    if email == 'prineshfarkya@gmail.com' and password == '123456':
        print(email)
        token = jwt.encode({
            'public_id': email,
            'exp': datetime.utcnow() + timedelta(minutes=30)
        }, app.config['SECRET_KEY'])
        print('success')
        return make_response(jsonify({'token': token.decode('UTF-8')}), 201)

    return make_response(jsonify("not a valid user"), 403)


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        # jwt is passed in the request header
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        # return 401 if token is not passed
        if not token:
            return jsonify({'message': 'Token is missing !!'}), 401
        return f('user found')

    return decorated

