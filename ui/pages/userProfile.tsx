import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Layout from '../src/Components/layout'
import styles from '../styles/Home.module.css'
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import EditIcon from '@mui/icons-material/Edit';
import { Box, Grid, Typography, Card, CardActionArea, CardHeader, CardContent } from '@mui/material'
import ImageStack from '../src/Components/common/imageStack';
import { Carousel } from '../src/Components/customControls/carouselControl';
import { getTopCollections } from '../src/Components/repository/nftRepo'
import UserProfileDetails from '../src/Components/user/userProfileDetails'

const UserProfile: NextPage = () => {
  return (
    <div className={styles.container}>
      <UserProfileDetails />
    </div>
  )
}

export default UserProfile
