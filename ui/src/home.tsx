import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Layout from './Components/layout'
import NftSingleDisplay from './Components/common/nftSingleDisplay'
import { getWishlisted, getRecents } from './Components/repository/primaryData'
import React, { useEffect } from "react";
import FavoriteIcon from '@mui/icons-material/Favorite';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import { getWishlist, getRecentlyViewed, getTopCollections } from './Components/repository/nftRepo';
import { getTop10Recommendations, getTopUpcomingCollections } from './Components/repository/primaryData';
import { Box, Grid, Typography, Card, CardActionArea, CardHeader, CardContent, CardMedia } from '@mui/material'
import { Carousel } from './Components/customControls/carouselControl';
import { randomInt } from 'crypto'
import NftCollectionDisplay from './Components/common/nftCollectionDisplay';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import RecommendIcon from '@mui/icons-material/Recommend';

const Home: NextPage = () => {

  const wishlistedIcon = <FavoriteIcon color="error" style={{ verticalAlign: "middle" }} />;
  const recentsIcon = <AccessTimeIcon color="success" style={{ verticalAlign: "middle" }} />;
  const [wishlisted, setWishlist] = React.useState<any | any>([]);
  const [recents, setRecents] = React.useState<any | any>([]);

  const defaultImage = "https://lh3.googleusercontent.com/82lDAsUEcWW8xuSuV_hLWq5jZn6li8FZ__okWbwqLp_ZZf8kvtYyTsP3Q-muu2tKpM4rul_AySGgli-PgFnjBqD2OeJpQu8Q3fYIZQ=s120";

  const colors = ['#D1C4E9', '#F8BBD0', '#FBE9E7', '#D7CCC8', '#E0F7FA', '#E0E0E0', '#607D8B', '#80DEEA'];

  useEffect(() => {
    async function getCollections() {
      if (wishlisted == undefined || wishlisted.length == 0) {
        const data = await getTopCollections();
        const recentData = await getTopUpcomingCollections();
        setWishlist(data);
        setRecents(recentData);
      }
    }
    getCollections();
  });
  return (
    <div className={styles.container} style={{ verticalAlign: 'center', marginTop: 5, height: '100%' }}>
      <Grid container>
        <Grid container marginTop='20px'>
          <Grid item>
            <h2>Trending Nfts Collections</h2>
          </Grid>
          <Grid item marginTop={3.5} marginLeft={1}>
            <TrendingUpIcon sx={{ color: 'red', alignSelf: 'center' }} />
          </Grid>
        </Grid>
        <Grid container marginTop='10px'>
          <Carousel show={5}>
            {wishlisted.map((item: any) => (
              <div style={{ width: 350, height: 300, marginLeft: 5 }} key={item.Name}>
                <Box >
                  <Card onClick={() => window.open("https://opensea.com", "_blank")}>
                    <CardActionArea>
                      <CardHeader title={`${item.Name}`}
                        titleTypographyProps={{ fontSize: 16, fontFamily: 'Calibri', fontWeight: 'Bold' }}
                        style={{ textAlign: 'center', background: colors[Math.floor(Math.random() * colors.length)], width: '100%', height: 40 }} />
                      <CardContent >
                        <CardMedia component='img' height='200' image={item.ImageUrl == "" || item.ImageUrl == null ? defaultImage : item.ImageUrl} alt={item.Name} />
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </Box>
              </div>
            ))}
          </Carousel>
        </Grid>
        <Grid container marginTop='20px'>
          <Grid item>
            <h2>Top Recommended in Primary </h2>
          </Grid>
          <Grid item marginTop={3.5} marginLeft={1}>
            <RecommendIcon sx={{ color: 'green', alignSelf: 'center' }} />
          </Grid>
        </Grid>
        <Grid container marginTop='10px'>
          <Carousel show={5}>
            {recents.map((item: any) => (
              <div style={{ width: 350, height: 300, marginLeft: 5 }} key={item.Name}>
                <Box >
                  <Card onClick={() => window.open("https://opensea.com", "_blank")}>
                    <CardActionArea>
                      <CardHeader title={`${item.Name}`}
                        titleTypographyProps={{ fontSize: 16, fontFamily: 'Calibri', fontWeight: 'Bold' }}
                        style={{ textAlign: 'center', background: colors[Math.floor(Math.random() * colors.length)], width: '100%', height: 40 }} />
                      <CardContent >
                        <CardMedia component='img' height='200' image={item.ImageUrl == "" || item.ImageUrl == null ? defaultImage : item.ImageUrl} alt={item.Name} />
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </Box>
              </div>
            ))}
          </Carousel>
        </Grid>
      </Grid>
    </div>
  )
}

export default Home
