import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import EditIcon from '@mui/icons-material/Edit';
import { Box, Grid, Typography, Card, CardActionArea, CardHeader, CardContent, CardMedia } from '@mui/material'
import ImageStack from '../common/imageStack';
import { Carousel } from '../customControls/carouselControl';
import { getWishlist } from '../repository/nftRepo'
import { useEffect } from 'react';
import React from 'react';
import Chip from '@mui/material/Chip';
import FavoriteIcon from '@mui/icons-material/Favorite';
import Link from '@mui/material/Link';

export default function UserProfileDetails() {
    const [wishlist, setWishlist] = React.useState<any | any>([]);

    const defaultImage = "https://lh3.googleusercontent.com/82lDAsUEcWW8xuSuV_hLWq5jZn6li8FZ__okWbwqLp_ZZf8kvtYyTsP3Q-muu2tKpM4rul_AySGgli-PgFnjBqD2OeJpQu8Q3fYIZQ=s120";

    const colors = [ '#D1C4E9', '#F8BBD0', '#FBE9E7', '#D7CCC8', '#E0F7FA', '#E0E0E0', '#607D8B', '#80DEEA' ];

    useEffect(() => {
        async function getCollections() {
            const data = await getWishlist();
            setWishlist(data);
        }
        getCollections();
    });

    const handleDelete =
      (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
        
      };

    return (
        <div style={{ verticalAlign: 'center', marginTop: 10, height: '100%' }}>
            <Grid container>
                <Grid key="Left" container sm={3} >
                    <Grid container>
                        <img width='200px' height='200px' src='/images/UserProfileDisplay.jpg' />
                    </Grid >
                    <Grid container sx={{marginTop:-10}}>
                        <h2>Malti Marie Jonas</h2>
                    </Grid>
                    <Grid container sx={{marginTop:-10}}>
                        <Typography fontFamily='Helvetica' color='blue' fontSize='14'>Change Password</Typography>
                    </Grid>
                    <Grid container >   
                        <Grid item>
                            <h2>Interests</h2>
                        </Grid>
                        <Grid item marginTop={3.5} marginLeft={1}>
                            <EditIcon sx={{color: 'primary'}} />
                        </Grid>                               
                    </Grid>                    
                    <Grid container marginTop='-75px'>
                        <Chip label="Art" variant="outlined" onDelete={handleDelete} sx={{ marginRight: 1, fontWeight: 'bold' }} />
                        <Chip label="Music" variant="outlined" onDelete={handleDelete} sx={{ marginRight: 1, fontWeight: 'bold' }} />
                        <Chip label="Sport" variant="outlined" onDelete={handleDelete} sx={{ marginRight: 1, fontWeight: 'bold' }} />
                    </Grid>
                </Grid>
                <Grid key="Right" container sm={9} rowSpacing={0}>
                    <Grid container >   
                        <Grid item>
                            <h2>Wallet :</h2>
                        </Grid>
                        <Grid item marginTop={3.5} marginLeft={1}>
                            <Typography fontFamily='Helvetica' fontSize='14' color='goldenrod' fontWeight='bold' marginLeft='15px'>
                                0xf76179bb0924ba7da8e7b7fc2779495d7a7939d8
                            </Typography>
                        </Grid>                               
                    </Grid>     
                    <Grid container >   
                        <Grid item>
                            <h2>Wishlist</h2>
                        </Grid>
                        <Grid item marginTop={3.25} marginLeft={1}>
                            <FavoriteIcon sx={{ color: 'red' }} />
                        </Grid>                               
                    </Grid>                   
                    <Grid container>
                        <Carousel show={5}>
                            {wishlist.map((item: any) => (
                                <div style={{ width: 200, height: 250 }} key={item.Name}>
                                    <Box >
                                        <Card onClick={() => window.open("https://opensea.com", "_blank")}>
                                            <CardActionArea>
                                                <CardHeader title={`${item.Name}`}
                                                    titleTypographyProps={{ fontSize: 16, fontFamily: 'Helvetica', fontWeight: 'Bold' }}
                                                    style={{ textAlign: 'center', background: colors[Math.floor(Math.random()*colors.length)], width: '100%', height: 40 }} />
                                                <CardContent >
                                                    <CardMedia component='img' height='110' image={item.ImageUrl == "" || item.ImageUrl == null ? defaultImage : item.ImageUrl} alt={item.Name} />
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Box>
                                </div>
                            ))}
                        </Carousel>
                    </Grid>
                    <Grid container>
                        <h2>My Market Places</h2>
                    </Grid>
                    <div style={{marginBottom: 100}}>
                        <li style={{margin: 5}} >
                            <Link href="https://www.opensea.com" >
                                Opensea
                            </Link>
                        </li>
                        <li style={{margin: 5}} ><Link href="https://www.binance.com/en/nft/home">Binance NFT</Link></li>
                        <li style={{margin: 5}} ><Link href="https://rarible.com/">Rarible</Link></li>                        
                    </div>
                </Grid>
            </Grid >
        </div >
    )
}