import axios from "axios";
import { ApiBaseUri } from '../../config'
const upcoming = [
  {
    creator: "Crypto Dragonies",
    description: "Pixelated dragon themed NFTs",
    color: "#D1C4E9",
    imageData: [
      { url: "./cav.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 2,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "3dpunki3D",
    description: "avatar NFTs",
    color: "#F8BBD0",
    imageData: [
      { url: "./3d.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 3,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "The Last DreamStreet",
    description: "Art Inspired NFTs",
    color: "#FBE9E7",
    imageData: [
      { url: "./lost.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 4,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "Dream Girls",
    description: "NFTPretty Girls Themed NFTs",
    color: "#D7CCC8",
    imageData: [
      { url: "./girl.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "Covheads",
    description: "Virus themed NFTs",
    color: "#E0F7FA",
    imageData: [
      { url: "./corona.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "OnÃ§a",
    description: "Cryptoaguar themed NFTs",
    color: "#E0E0E0",
    imageData: [
      { url: "./ona.jpg", title: "Breakfast" },
    ],
    detailData: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "The Acid Walrus",
    description: "Walrus Themed NFT",
    color: "#607D8B",
    imageData: [
      { url: "./acid.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "Cool Astronauts",
    description: "Astronaut themed NFTs",
    color: "#80DEEA",
    imageData: [
      { url: "./cool.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  }

];

const top = [
  {
    creator: "CavernCores",
    description: "NFT Ownership of Hardware",
    color: "#D1C4E9",
    imageData: [
      { url: "./cav.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 2,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "Mythicals",
    description: "Dragon themed NFTs",
    color: "#F8BBD0",
    imageData: [
      { url: "./myt.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 3,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "Bulls and Apes",
    description: "Bulls & Apes based NFTs",
    color: "#FBE9E7",
    imageData: [
      { url: "./bulls.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 4,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "TACTUS",
    description: "NFTCactus themed NFTs",
    color: "#D7CCC8",
    imageData: [
      { url: "./tac.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "Batty Banties",
    description: "Rooster themed NFTs",
    color: "#E0F7FA",
    imageData: [
      { url: "./batt.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "DONKEYKICKS",
    description: "Donkey Sneakers Themed NFTs",
    color: "#E0E0E0",
    imageData: [
      { url: "./loki.jpg", title: "Breakfast" },
    ],
    detailData: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "The Lost Bitcoins",
    description: "Grave stones themed NFTs",
    color: "#607D8B",
    imageData: [
      { url: "./lost.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  },
  {
    creator: "Aithereal",
    description: "AI Generative based NFTs",
    color: "#80DEEA",
    imageData: [
      { url: "./kith.jpg", title: "Breakfast" },
    ],
    details: {
      creatorProfileRating: 1,
      codeQualityRating: 2,
      socialMediaRating: 5
    }
  }

];

export function getTop10Recommendations() {
  let top10Recommendations = [];
  top10Recommendations = top;
  return top10Recommendations;
}

export function getUpcomingRecommendations() {
  let upcomingRecommendations = [];
  upcomingRecommendations = upcoming;
  return upcomingRecommendations;
}

export function getWishlisted() {
  let wishlisted = [];
  wishlisted = upcoming;
  return wishlisted;
}

export function getRecents() {
  let recents = [];
  recents = upcoming;
  return recents;
}

export async function getTopUpcomingCollections() {
  var response = await axios.get(`${ApiBaseUri}/top-upcoming-collections`)
  return response.data;
}