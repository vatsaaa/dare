import axios from "axios";
import { ApiBaseUri } from '../../config'


export async function getNftDetailsById(id) {
    var response = await axios.get(`${ApiBaseUri}/nft/${id}`)
    return response.data;
}

export async function getAllNft(d) {
    var response = await axios.get(`${ApiBaseUri}/nft`)
    return response.data;
}

export async function getTopCollections(d) {
    var response = await axios.get(`${ApiBaseUri}/top-collections/`)
    return response.data;
}


export async function getWishlist(d) {
    var response = await axios.get(`${ApiBaseUri}/wishlist/test`)
    return response.data;
}


export async function getRecentlyViewed(d) {
    var response = await axios.get(`${ApiBaseUri}/recently-viewed/test`)
    return response.data;
}

export async function getTopNft(d) {
    var response = await axios.get(`${ApiBaseUri}/top-nft`)
    return response.data;
}