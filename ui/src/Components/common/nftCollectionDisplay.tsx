import React from "react";
import Grid from '@mui/material/Grid';
import { Card, Box } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CardActionArea from '@mui/material/CardActionArea';
import ImageStack from '../common/imageStack';
import { Carousel } from '../customControls/carouselControl';
import NftDetailDisplay from '../common/nftDetailDisplay';
import ButtonBase from '@mui/material/ButtonBase';

class NftCollectionDisplay extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
    this.state = {
      isLoading: false,
      isDetailVisible: false,
      selectedCreator: null,
      selectedDetail: [],
      recommendations: [],
      title: null
    }
    this.showDetails = this.showDetails.bind(this);
  }

  async componentDidMount() {
    this.setState({ isLoading: true })
    var recommendations = this.props.recommendations;
    var title = this.props.title;
    this.setState({ recommendations: recommendations, isLoading: false })
  }

  // componentWillReceiveProps(props: any) {
  //   {
  //     this.refreshDetailVisible(false)
  //   }
  // }

  swapDetailVisible = () => this.setState({ isDetailVisible: !this.state.isDetailVisible })

  refreshDetailVisible = (val: any) => this.setState({ isDetailVisible: val })

  refreshSelectedDetail = (details: any) => this.setState({ selectedDetail: details })

  refreshSelectedCreator = (creator: any) => this.setState({ selectedCreator: creator })

  showDetails(item: any, event: any) {

    if (this.state.selectedCreator == item.creator) {
      this.swapDetailVisible();
    }
    else {
      this.refreshDetailVisible(true);
    }
    this.refreshSelectedCreator(item.creator);
    this.refreshSelectedDetail(item);

  }

  render() {
    return (
      <div>
        <Grid container item sx={{ padding: 1 }} >
          <Carousel show={5}>
            {this.props.recommendations.map((item: any) => (
              <div style={{ width: 120, height: 260, marginLeft: 5 }} key={item.name}>
                <Box border={item.creator === this.state.selectedDetail.creator ? 3 : 0} borderColor='orange'>
                  <Card onClick={(event) => this.showDetails(item, event)} >
                    <CardActionArea>
                      <CardHeader title={`${item.creator}`}
                        titleTypographyProps={{ fontSize: 16, fontFamily: 'Helvetica', fontWeight: 'Bold' }}
                        style={{ textAlign: 'center', backgroundColor: item.color, width: '100%', height: 40 }} />
                      <CardContent >
                        <Grid container justifyContent='center' >
                          <ImageStack imageData={item.imageData} />
                        </Grid>
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </Box>
              </div>
            ))}
          </Carousel>
          <div style={{ alignContent: 'center', margin: 10 }}>
            {this.state.isDetailVisible && <NftDetailDisplay selectedDetail={this.state.selectedDetail} key={this.state.selectedCreator} />}
          </div>
        </Grid>
      </div>
    )
  }
}

export default NftCollectionDisplay; 