import React from "react";
import Grid from '@mui/material/Grid';
import { Card, Box } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CardActionArea from '@mui/material/CardActionArea';
import { Carousel } from '../customControls/carouselControl';

class NftSingleDisplay extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            isLoading: false,
            text: "",
            icon: "",
            nftData: []
        }
    }
  
    async componentDidMount() {
        this.setState({ isLoading: true })        
        var nftData = this.props.nftData;
        var text = this.props.text;
        const Icon = this.props.icon;
        this.setState({ nftData: nftData, isLoading: false })   
    }
    

    render() {
        return (
            <div>
                <h2>
                    {this.props.text} {this.props.icon}
                </h2>            

                <Grid container item sx={{ padding: 1 }} >
                    <Carousel show={5}>                        
                        {this.props.nftData.map((item: any) => (
                            <div style={{ width: 120, height: 220, marginLeft: 5 }} key={item.name}>
                                <Box>
                                    <Card >
                                        <CardActionArea>
                                            <CardHeader title={`${item.creator}`}
                                                        titleTypographyProps={{ fontSize: 16, fontFamily: 'Calibri', fontWeight: 'Bold' }}
                                                        style={{ textAlign: 'center', backgroundColor: item.color, width: '100%', height: 40 }} />
                                            <CardContent >
                                                <Grid container justifyContent='center' >
                                                    <img src={`${item.imageData[0].url}?w=120&h=120&fit=crop&auto=format`} />
                                                </Grid>
                                            </CardContent>
                                        </CardActionArea>
                                    </Card>
                                </Box>
                            </div>
                        ))}
                    </Carousel>                    
                </Grid>
            </div>
           
        )
    }
}

export default NftSingleDisplay;