import * as React from 'react';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';

const ImageStack: any = ({ imageData }: { imageData: any }) => {
    console.log(imageData);
    return (
        <ImageList cols={1}>
            {imageData.map((item: any) => (
                <ImageListItem key={item.img}>
                    <img style={{ width: 150, height: 150 }}
                        src={`${item.url}?w=50&h=50&fit=crop&auto=format`}
                        srcSet={`${item.url}?w=50&h=50&fit=crop&auto=format&dpr=2 2x`}
                        alt={item.title}
                        loading="lazy"                       
                    />
                </ImageListItem>
            ))}
        </ImageList>
    );
};

export default ImageStack;

