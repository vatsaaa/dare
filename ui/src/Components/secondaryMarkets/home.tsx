import {
    CircularProgress, getAccordionActionsUtilityClass, TableContainer, Table, TableHead, TableRow, TableCell, TableBody,
    Typography, Card, CardMedia, CardContent, CardHeader, Avatar, IconButton, Grid, Accordion, AccordionDetails, AccordionSummary, Divider, Box
} from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import React from 'react';
import { getTopCollections, getTopNft } from '../repository/nftRepo'
import { Carousel } from '../customControls/carouselControl'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useEffect } from 'react';
import NftDetails from './nftDetails';
export default function SecondaryMarketsHome() {

    const [expanded, setExpanded] = React.useState<string | false>(false);
    const [assets, setAseets] = React.useState<any | any>([]);
    const [isLoading, setBusy] = React.useState<boolean | boolean>(false);
    const [currentSelected, setSelected] = React.useState<any | any>(null);

    const defaultImage = "https://lh3.googleusercontent.com/82lDAsUEcWW8xuSuV_hLWq5jZn6li8FZ__okWbwqLp_ZZf8kvtYyTsP3Q-muu2tKpM4rul_AySGgli-PgFnjBqD2OeJpQu8Q3fYIZQ=s120";

    const colors = ['#D1C4E9', '#F8BBD0', '#FBE9E7', '#D7CCC8', '#E0F7FA', '#E0E0E0', '#607D8B', '#80DEEA'];

    useEffect(() => {
        setBusy(true);
        async function getCollections() {
            if (assets == undefined || assets.length == 0) {
                const data = await getTopNft();
                setAseets(data);
            }
        }
        getCollections();
        setBusy(false);

    });

    const handleChange =
        (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
            setExpanded(isExpanded ? panel : false);
        };

    const onCollectionDetails = (collection: any, event: any) => {
        setSelected(collection);
    }

    return (
        <div style={{ verticalAlign: 'center', marginTop: 5 }}>
            {isLoading ? (<>
                <p style={{ verticalAlign: 'center' }}>
                    <CircularProgress style={{ verticalAlign: 'center' }} />
                </p>
            </>) : (<>
                <div style={{ width: '100%' }}>
                    <Accordion expanded={expanded === 'top10'} onChange={handleChange('top10')} sx={{ marginTop: 1 }}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon sx={{ color: 'whitesmoke' }} />} aria-controls="panel1a-content" id="panel1a-header"
                            sx={{ backgroundColor: '#4A148C', color: 'whitesmoke', fontSize: 20, padding: 1, height: 22 }}>
                            Top Recommendations
                        </AccordionSummary>
                        <AccordionDetails>
                            <Carousel show={5}>
                                {/* <Grid container spacing={4}> */}
                                {
                                    assets.map((row: any) =>
                                    (
                                        <div key={row.name} style={{ width: 200, height: 250, marginLeft: 5 }} >
                                            <Box border={(currentSelected != undefined || currentSelected !== null) && row.ID === currentSelected.ID ? 2 : 0} marginTop={1} borderColor='orange'>
                                                <Card
                                                    onClick={(event) => onCollectionDetails(row, event)}>
                                                    <CardHeader
                                                        title={row.Name}
                                                        titleTypographyProps={{ fontSize: 16, fontFamily: 'Helvetica', fontWeight: 'Bold' }}
                                                        style={{ textAlign: 'center', background: colors[Math.floor(Math.random() * colors.length)], width: '100%', height: 40 }} />
                                                    <CardContent >
                                                        <CardMedia component='img' height='130' image={row.ImageUrl == "" || row.ImageUrl == null ? defaultImage : row.ImageUrl} alt={row.Name} />
                                                    </CardContent>
                                                </Card>
                                            </Box>
                                        </div>
                                    ))
                                }
                                {/* </Grid> */}
                            </Carousel>
                            {/* <Divider color='orange' style={{ marginTop: '8px', marginBottom: '8px' }} /> */}
                            <Grid >
                                {currentSelected != null && (<NftDetails nftItem={currentSelected} />)}
                            </Grid>
                        </AccordionDetails>
                    </Accordion>
                </div>
            </>)
            }
        </div>)
}
