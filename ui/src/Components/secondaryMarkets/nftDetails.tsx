import { Grid, Typography, Box, Button } from "@mui/material";
import StarIcon from '@mui/icons-material/Star';
import FavoriteIcon from '@mui/icons-material/Favorite';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import LanguageIcon from '@mui/icons-material/Language';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import { textAlign, width } from "@mui/system";
export default function NftDetails(props: any) {
    const { nftItem } = props;

    return (<>
        <Grid container id="main" rowSpacing={5} marginTop={-8} >
            <Grid container id="left" xs={12} sm={8} item rowSpacing={5} >
                <Grid item container id="header">
                    <Grid item xs={12} sm={1.2}>
                        <img src={nftItem.ImageUrl} style={{ width: "100px", height: "100px", marginTop: '10px' }} />
                    </Grid>
                    <Grid item xs={12} sm={9} >
                        <h2>{nftItem.Name} </h2>
                        <h4>{nftItem.Description}</h4>
                    </Grid>
                </Grid>

                <Grid item id="financialdetails" container>
                    <Grid container xs={12} sm={4} marginTop={-2} >
                        <Grid item>
                            <img src='/Ether.jpg' style={{ width: '50px', height: '50px', marginRight: 5 }} />
                        </Grid>
                        <Grid item marginTop={-1} marginLeft={1}>
                            <h2>{`${nftItem.TradingDetails[0].AvgPrice7days}`}</h2>
                        </Grid>
                    </Grid>

                    <Grid id="traits" container marginTop={-4}>
                        <Grid container marginTop='10px'>
                            <h2>Properties</h2>
                        </Grid>
                        {
                            nftItem.Traits.map((t: any) =>
                            (<Grid item xs={12} sm={3} key={t.trait_type}>
                                <Box sx={{ border: 1, borderRadius: 2, borderColor: '#AF9595', alignContent: 'center', background: '#F3F2F2', height: '55px', width: '275px', marginTop: '7px' }}>
                                    <Typography alignContent='center' textAlign='center' fontFamily='Helvetica' color='#AF9595' >{t.trait_type}</Typography>
                                    <Typography alignContent='center' fontWeight='bold' textAlign='center' fontFamily='Helvetica' color='#AF9595' >{t.value}</Typography>
                                </Box>
                            </Grid>)
                            )
                        }
                    </Grid>
                </Grid>

                <Grid id="numbers" container>

                    <Grid container >
                        <Grid item>
                            <h4>Floor Price : </h4>
                        </Grid>
                        <Grid item marginTop={2.75} marginLeft={1}>
                            {`${nftItem.TradingDetails[0].AvgPrice7days}`}
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginTop: -30 }}>
                        <Grid item>
                            <h4>Trade Volumes : </h4>
                        </Grid>
                        <Grid item marginTop={2.75} marginLeft={1}>
                            {`${nftItem.TradingDetails[0].VolumeAllTime}`}
                        </Grid>
                    </Grid>

                </Grid>

                <Grid item id="social Media" container rowSpacing={5}>
                    <Grid container xs={12} sm={2} style={{ marginTop: 0 }}>
                        <Grid item>
                            <TwitterIcon color="primary" />
                        </Grid>
                        <Grid item marginTop={0} marginLeft={1}>
                            <a href={`https://twitter.com/${nftItem.ProjectSocialMedia.Twitter.Handle}`} target="_blank" rel="noreferrer">{nftItem.ProjectSocialMedia.Twitter.Handle}</a>
                        </Grid>
                    </Grid>
                    <Grid container xs={12} sm={2} style={{ marginTop: 0 }}>
                        <Grid item>
                            <InstagramIcon color="error" />
                        </Grid>
                        <Grid item marginTop={0} marginLeft={1}>
                            <a href={`https://instagram.com/${nftItem.ProjectSocialMedia.Instagram.Handle}`} target="_blank" rel="noreferrer">{nftItem.ProjectSocialMedia.Instagram.Handle}</a>
                        </Grid>
                    </Grid>
                    <Grid container xs={12} sm={2} style={{ marginTop: 0 }}>
                        <Grid item>
                            <SportsEsportsIcon color="primary" />
                        </Grid>
                        <Grid item marginTop={0} marginLeft={1}>
                            <a href={`https://discord.com/${nftItem.ProjectSocialMedia.Discord.Handle}`} target="_blank" rel="noreferrer">{nftItem.ProjectSocialMedia.Discord.Handle}</a>
                        </Grid>
                    </Grid>
                </Grid>

            </Grid>

            <Grid container item id="right" xs={12} sm={4} >

                <Grid id="numbers" container>

                    <Grid container >
                        <Grid item>
                            <StarIcon color="primary" style={{ width: 50, height: 50 }} />
                        </Grid>
                        <Grid item marginTop={-1} marginLeft={1}>
                            <h2>{`${nftItem.TrusthWorthiness.DARE}`}</h2>
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginTop: 0 }}>
                        <Grid item>
                            <FavoriteIcon color="error" style={{ width: 45, height: 45 }} />
                        </Grid>
                        <Grid item marginTop={-2} marginLeft={2}>
                            <h2>260</h2>
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginTop: -10 }}>
                        <Grid item>
                            <h2><div style={{ color: 'green' }}>BUY</div></h2>
                        </Grid>
                        <Grid item>
                            <h2>/</h2>
                        </Grid>
                        <Grid item>
                            <h2><div style={{ color: 'red' }}>SELL&nbsp;</div></h2>
                        </Grid>
                        <Grid item>
                            <h2> :</h2>
                        </Grid>
                        <Grid item marginTop={0} marginLeft={1}>
                            <h2 style={{ color: 'green' }} >BUY</h2>
                        </Grid>
                    </Grid>

                </Grid>

                <Grid item id="tradebuttons" container>
                    <Box sx={{ border: 1, borderRadius: 2, borderColor: 'Gray', alignContent: 'center', height: '150px', width: '300px' }}>
                        <h3 style={{ textAlign: 'center' }}>Trade</h3>
                        <Grid item container marginLeft='40px' marginTop='10px'>
                            <Grid item xs={12} sm={5}>
                                <Button variant="contained" color="success" style={{ width: '100px', alignSelf: 'center', borderRadius: 5 }}>
                                    Markets
                                </Button>
                            </Grid>
                            <Grid item xs={12} sm={7} >
                                <Button variant="contained" color="info" style={{ width: '100px', alignSelf: 'center' }}>
                                    P2P
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>

                <Grid id="contactDetails1" container>
                    <Grid container >
                        <Grid item xs={12} sm={3}>
                            <h4>Contract Address :</h4>
                        </Grid>
                        <Grid item marginTop={2.75} marginLeft={1} xs={12} sm={8}>
                            {nftItem.ContractAddress}
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginTop: -45 }}>
                        <Grid item xs={12} sm={3}>
                            <h4>Token Id :</h4>
                        </Grid>
                        <Grid item marginTop={2.75} marginLeft={1} xs={12} sm={8}>
                            {`${nftItem.TokenId}`}
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginTop: -45 }}>
                        <Grid item xs={12} sm={3}>
                            <h4>Token Standard :</h4>
                        </Grid>
                        <Grid item marginTop={2.75} marginLeft={1} xs={12} sm={8}>
                            ERC-721
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginTop: -45 }} >
                        <Grid item xs={12} sm={3}>
                            <h4>Blockchain :</h4>
                        </Grid>
                        <Grid item marginTop={2.75} marginLeft={1} xs={12} sm={8}>
                            Ethereum
                        </Grid>
                    </Grid>
                </Grid>

            </Grid>
        </Grid >
    </>)
}